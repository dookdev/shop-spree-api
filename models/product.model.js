module.exports = function (mongoose) {
    const Schema = mongoose.Schema;

    var productScrema = new Schema({
        name: {
            type: String,
            required: [true, 'name is required'],
            trim: true
        },
        picture: {
            type: String,
            required: [true, 'picture is required'],
            trim: true
        },
        quantity: {
            type: Number,
            min: 0,
            default: 0,
            trim: true
        },
        price: {
            type: Number,
            min: 0,
            default: 0,
            trim: true
        }
    });

    mongoose.model('product', productScrema);
};