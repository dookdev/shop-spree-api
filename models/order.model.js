module.exports = function (mongoose) {
    const Schema = mongoose.Schema;

    var orderScrema = new Schema({
        products: [{
            id: {
                type: Schema.ObjectId,
                ref: 'product',
                trim: true
            },
            qty: {
                type: Number,
                trim: true
            }
        }],
        address: {
            type: String,
            required: [true, 'address is required'],
            trim: true
        },
        email: {
            type: String,
            required: [true, 'email is required'],
            trim: true
        },
        firstName: {
            type: String,
            required: [true, 'firstName is required'],
            trim: true
        },
        lastName: {
            type: String,
            required: [true, 'lastName is required'],
            trim: true
        },
        phoneNumber: {
            type: String,
            required: [true, 'phoneNumber is required'],
            trim: true
        },
        totalPrice: {
            type: Number,
            min: 0,
            default: 0,
            trim: true
        },
        vat: {
            type: Number,
            min: 0,
            default: 0,
            trim: true
        },
        net: {
            type: Number,
            min: 0,
            default: 0,
            trim: true
        },
        createDate: { type: Date, default: Date.now }
    });

    mongoose.model('order', orderScrema);
};