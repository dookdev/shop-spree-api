var express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    glob = require('glob'),
    path = require('path'),
    http = require('http'),
    mongoose = require('mongoose');

var app = express();
var router = express.Router();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api', router);

mongoose.connect('mongodb://localhost:27017/shop-spree-db', { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.set('debug', true);
mongoose.set('useFindAndModify', false);

glob.sync(path.join(__dirname, '**/models/*.model.js')).forEach(function (file) {
    require(path.resolve(file))(mongoose);
});

glob.sync(path.join(__dirname, '**/routes/*.route.js')).forEach(function (file) {
    require(path.resolve(file))(router);
});

var server = http.createServer(app);
server.listen(3001, () => {
    console.log('Listening on port 3001');
});