const productCtrl = require('../controllers/product.controller');
module.exports = function (router) {
    router.route('/')
        .get(productCtrl.hello)
        .post(productCtrl.hello)
        .put(productCtrl.hello)
        .delete(productCtrl.hello);

    router.route('/getProducts')
    .get(productCtrl.getProducts);

    router.route('/createProduct')
    .post(productCtrl.createProduct);
};