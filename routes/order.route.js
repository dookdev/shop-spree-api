const orderCtrl = require('../controllers/order.controller');
module.exports = function (router) {
    router.route('/createOrder')
    .post(orderCtrl.createOrder);
};