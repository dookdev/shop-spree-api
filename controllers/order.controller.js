const orderModel = require('mongoose').model('order');
const productModel = require('mongoose').model('product');
const handle = require('../utilitys/handle');

exports.createOrder = async function (req, res) {
    valodateQty(req.body.products).then(dd => {
        var order = new orderModel(req.body);
        order.save(function (err, result) {
            if (err) {
                res.status(404).json(handle.error(err));
            } else {
                req.body.products.forEach(async (item) => {
                    await productModel.findByIdAndUpdate({ _id: item.id }, { $inc: { quantity: -(item.qty) } });
                });
                res.json(handle.success(result));
            }
        });
    }).catch(err => {
        res.status(404).json(handle.error('Qty incorrect'));
    });
};

function valodateQty(params) {
    return new Promise((resolve, reject) => {
        params.forEach(async (item, i) => {
            var rs = await productModel.findOne({ _id: item.id }, 'quantity');
            if (rs && rs.quantity) {
                if ((rs.quantity - item.qty) < 0) {
                    reject(false);
                } else {
                    if ((i + 1) === params.length) {
                        resolve(true);
                    }
                }
            } else {
                reject(false);
            }
        });
    });
}