const productModel = require('mongoose').model('product');
const handle = require('../utilitys/handle');

exports.hello = function (req, res) {
    res.send('hello api');
};

exports.getProducts = function (req, res) {
    productModel.find().exec(function (err, result) {
        if (err) {
            res.status(404).json(handle.error(err));
        } else {
            var productList = [];
            result.forEach((item) => {
                productList.push({
                    id: item._id,
                    picture: item.picture,
                    name: item.name,
                    price: item.price,
                    quantity: item.quantity

                });
            });
            res.json(handle.success(productList));
        }
    });
};

exports.createProduct = function (req, res) {
    var product = new productModel(req.body);
    product.save(function (err, result) {
        if (err) {
            res.status(404).json(handle.error(err));
        } else {
            res.json(handle.success(result));
        }
    });
};